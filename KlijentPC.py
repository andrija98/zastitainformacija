import socket
import pickle
import Crypto
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import PKCS1_OAEP
import ast

def rsa_encrypt_decrypt():
    key = RSA.generate(2048)
    private_key = key.export_key('PEM')
    public_key = key.publickey().exportKey('PEM')
    message = input('plain text for RSA encryption and decryption:')
    message = str.encode(message)

    rsa_public_key = RSA.importKey(public_key)
    rsa_public_key = PKCS1_OAEP.new(rsa_public_key)
    encrypted_text = rsa_public_key.encrypt(message)
    #encrypted_text = b64encode(encrypted_text)

    print('your encrypted_text is : {}'.format(encrypted_text))


    rsa_private_key = RSA.importKey(private_key)
    rsa_private_key = PKCS1_OAEP.new(rsa_private_key)
    decrypted_text = rsa_private_key.decrypt(encrypted_text)

    print('your decrypted_text is : {}'.format(decrypted_text))

key = RSA.generate(2048)
private_key = key.export_key('PEM')
public_key = key.publickey().exportKey('PEM')


HOST = '127.0.0.1'
PORT = 12345

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect((HOST, PORT))
    s.sendall(public_key)
    pubKeyServer = s.recv(1024)
    print(pubKeyServer)
    javniKljucServera = pubKeyServer
    while True:
        rsa_public_key = RSA.importKey(javniKljucServera)
        rsa_public_key = PKCS1_OAEP.new(rsa_public_key)
        poruka = input('Unesi poruku serveru: ')
        poruka = str.encode(poruka)
        encrypted_text = rsa_public_key.encrypt(poruka)
        odgovor = pickle.dumps(encrypted_text)
        s.sendall(odgovor)
        data = s.recv(1024)
        odgovorByte = pickle.loads(data)

        rsa_private_key = RSA.importKey(private_key)
        rsa_private_key = PKCS1_OAEP.new(rsa_private_key)
        decrypted_text = rsa_private_key.decrypt(odgovorByte)

        print("Server: ", decrypted_text.decode())




