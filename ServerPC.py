import socket
import pickle
import Crypto
from Crypto.PublicKey import RSA
from Crypto import Random
from Crypto.Cipher import PKCS1_OAEP
import ast

def rsa_encrypt_decrypt():
    key = RSA.generate(2048)
    private_key = key.export_key('PEM')
    public_key = key.publickey().exportKey('PEM')
    message = input('plain text for RSA encryption and decryption:')
    message = str.encode(message)

    rsa_public_key = RSA.importKey(public_key)
    rsa_public_key = PKCS1_OAEP.new(rsa_public_key)
    encrypted_text = rsa_public_key.encrypt(message)
    #encrypted_text = b64encode(encrypted_text)

    print('your encrypted_text is : {}'.format(encrypted_text))


    rsa_private_key = RSA.importKey(private_key)
    rsa_private_key = PKCS1_OAEP.new(rsa_private_key)
    decrypted_text = rsa_private_key.decrypt(encrypted_text)

    print('your decrypted_text is : {}'.format(decrypted_text))

key = RSA.generate(2048)
private_key = key.export_key('PEM')
public_key = key.publickey().exportKey('PEM')

HOST = '192.168.1.2'
PORT = 12345


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    conn, addr = s.accept()

    with conn:
        print ('Connected by', addr)
        pubKeyClient = conn.recv(1024)
        javniKljucKlijent = pubKeyClient
        conn.sendall(public_key)
        while True:
            data = conn.recv(1024)
            if not data:
                break
            odgovor = pickle.loads(data)
            rsa_private_key = RSA.importKey(private_key)
            rsa_private_key = PKCS1_OAEP.new(rsa_private_key)
            decrypted_text = rsa_private_key.decrypt(odgovor)
            print("Klijent: ", decrypted_text.decode())

            rsa_public_key = RSA.importKey(javniKljucKlijent)
            rsa_public_key = PKCS1_OAEP.new(rsa_public_key)

            poruka = input('Unesi poruku klijentu: ')
            poruka = str.encode(poruka)
            encrypted_text = rsa_public_key.encrypt(poruka)
            odgovorByte = pickle.dumps(encrypted_text)
            conn.sendall(odgovorByte)

